import './App.scss';
import MyFirstComponent from "./components/MyFirstComponent/MyFirstComponent";
import {logDOM} from "@testing-library/react";

const arr = [1,2,3,"asdf"];

const arrP = arr.map(e=>{
  return <li>{e}</li>
})

const render = true;

const handler = (text)=> {
  console.log("btn")
  console.log(text)
};

function App() {


  return (
    <div className="app">

      <MyFirstComponent text={"Hello!"} fs={"55px"} prop={"asd"} name={"name"} buttonName={"Кнопка"} action={handler}/>

      {render&&<MyFirstComponent text={"Hello!"} fs={"55px"} prop={"asd"} name={"name"} buttonName={"Кнопка"} />}

      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus architecto consequatur dolor dolore dolorem
        expedita illum maiores nam nemo non omnis quam quas, quisquam ratione sed sint soluta vel velit!
      </p>

      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus architecto consequatur dolor dolore dolorem
        expedita illum maiores nam nemo non omnis quam quas, quisquam ratione sed sint soluta vel velit!
      </p>

      <ul> {arrP}</ul>


    </div>
  );
}

export default App;
