import React, {Component} from 'react';
import "./MyFirstComponent.scss"
import Button from "../Button/Button";

class MyFirstComponent extends Component {
  // constructor(props) {
  //   super(props);
  // }

  render() {

    const {fs,name,text,buttonName, action} = this.props

    const style={
      fontSize:fs,
    }

    return (
      <>
        <h2 style={style} > {this.props.text}</h2>
        <h1> Hello React!</h1>

        <Button action={action} name={buttonName}/>
      </>
    );
  }
}

export default MyFirstComponent;